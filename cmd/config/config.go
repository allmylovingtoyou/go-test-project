package config

type Config struct {
	Config *ToDoConfig
}

type ToDoConfig struct {
	CategoryFile string
	TaskFile     string
	Host         string
	Port         int
}

func GetConfig() *Config {
	return &Config{
		Config: &ToDoConfig{
			CategoryFile: "category.json",
			TaskFile:     "task.json",
			Host:         "127.0.0.1",
			Port:         8088,
		},
	}
}
