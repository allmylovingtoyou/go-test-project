package category

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/schollz/jsonstore"
	"net/http"
	"regexp"
	"testProject/cmd/config"
	"testProject/pkg/common"
)

var storage *jsonstore.JSONStore
var cfg *config.ToDoConfig

type Category struct {
	Id    uuid.UUID `json:"id"`
	Title string    `json:"title"`
}

func Add(writer http.ResponseWriter, request *http.Request) {
	cat := Category{}

	decoder := json.NewDecoder(request.Body)
	if err := decoder.Decode(&cat); err != nil {
		common.RespondError(writer, http.StatusBadRequest, err.Error())
		return
	}

	id := uuid.New()
	cat.Id = id
	_ = storage.Set(id.String(), cat)

	if err := jsonstore.Save(storage, cfg.CategoryFile); err != nil {
		panic(err)
	}

	common.RespondJSON(writer, http.StatusOK, cat)
}

func Update(writer http.ResponseWriter, request *http.Request) {
	cat := Category{}

	decoder := json.NewDecoder(request.Body)
	if err := decoder.Decode(&cat); err != nil {
		common.RespondError(writer, http.StatusBadRequest, err.Error())
		return
	}

	_ = storage.Set(cat.Id.String(), cat)

	if err := jsonstore.Save(storage, cfg.CategoryFile); err != nil {
		panic(err)
	}

	common.RespondJSON(writer, http.StatusOK, cat)
}

func GetById(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	param := vars["id"]

	id, done := uuidParamTryParse(param, writer)
	if done {
		return
	}

	cat := Category{}
	err := storage.Get(id.String(), &cat)
	if err != nil {
		common.RespondError(writer, http.StatusNotFound, err.Error())
		return
	}

	common.RespondJSON(writer, http.StatusOK, cat)
}

func Delete(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	param := vars["id"]

	id, done := uuidParamTryParse(param, writer)
	if done {
		return
	}

	storage.Delete(id.String())
	common.RespondJSON(writer, http.StatusNoContent, "")
}

func GetAll(writer http.ResponseWriter, request *http.Request) {
	reg, _ := regexp.Compile(".*")
	result := storage.GetAll(reg)
	common.RespondJSON(writer, 200, result)
}

func SetUp(config *config.ToDoConfig) {
	st, err := jsonstore.Open(config.CategoryFile)
	if err != nil {
		var ks = new(jsonstore.JSONStore)
		_ = ks.Set(uuid.New().String(), Category{Title: "Wife fucking car"})
		_ = ks.Set(uuid.New().String(), Category{Title: "Work"})

		if err := jsonstore.Save(ks, config.CategoryFile); err != nil {
			panic(err)
		}
	}
	st, _ = jsonstore.Open(config.CategoryFile)
	storage = st
	cfg = config
}

func uuidParamTryParse(param string, writer http.ResponseWriter) (uuid.UUID, bool) {
	id, err := uuid.Parse(param)
	if err != nil {
		common.RespondError(writer, http.StatusBadRequest, err.Error())
		return uuid.UUID{}, true
	}
	return id, false
}
