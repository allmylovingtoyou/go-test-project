package category

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testProject/cmd/config"
	"testing"
)

var testConfig *config.ToDoConfig

func TestAdd(t *testing.T) {
	testSetUp()
	cat := Category{Title: "testTitle"}

	b, _ := json.Marshal(&cat)
	reader := bytes.NewReader(b)

	request := httptest.NewRequest("POST", "https://stackoverflow.com", reader)
	recorder := httptest.NewRecorder()

	handler := http.HandlerFunc(Add)
	handler.ServeHTTP(recorder, request)

	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}

func testSetUp() {
	testConfig = &config.ToDoConfig{
		CategoryFile: "../../test/category_test.json",
	}
	err := os.Remove(testConfig.CategoryFile)
	if err != nil {
		panic(err)
	}

	SetUp(testConfig)
}
