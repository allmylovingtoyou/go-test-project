package httpGw

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"testProject/cmd/config"
	"testProject/pkg/category"
)

type HttpGw struct {
	Router *mux.Router
}

func (gw *HttpGw) Run(config *config.Config) {
	log.Fatal(http.ListenAndServe(getAddrString(config), gw.Router))
}

func (gw *HttpGw) Initialize(config *config.Config) {
	gw.Router = mux.NewRouter()
	gw.setRouters()
}

func (gw *HttpGw) setRouters() {
	gw.Post("/category", gw.handleRequest(category.Add))
	gw.Patch("/category", gw.handleRequest(category.Update))
	gw.Get("/category/getPage", gw.handleRequest(category.GetAll))
	gw.Get("/category/{id}", gw.handleRequest(category.GetById))
	gw.Delete("/category/{id}", gw.handleRequest(category.Delete))
}

func (gw *HttpGw) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	gw.Router.HandleFunc(path, f).Methods("DELETE")
}

func (gw *HttpGw) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	gw.Router.HandleFunc(path, f).Methods("GET")
}

func (gw *HttpGw) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	gw.Router.HandleFunc(path, f).Methods("POST")
}

func (gw *HttpGw) Patch(path string, f func(w http.ResponseWriter, r *http.Request)) {
	gw.Router.HandleFunc(path, f).Methods("PATCH")
}

type RequestHandlerFunction func(w http.ResponseWriter, r *http.Request)

func (gw *HttpGw) handleRequest(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		handler(w, r)
	}
}

func getAddrString(config *config.Config) string {
	return config.Config.Host + ":" + strconv.Itoa(config.Config.Port)
}
