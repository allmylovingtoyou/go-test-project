module testProject

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/schollz/jsonstore v1.1.0
)
